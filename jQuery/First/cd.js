<script>
    $(document).ready(function () {
        $(".btn_start").click(function () {
            $(".status").val("RUNNING").css("color", "green").css("border-color", "green").css("text-align", "center");

            var seconds = $(".enter_seconds").val();
            //  console.log(seconds);
            var h = Math.floor(seconds / 3600);
            var m = Math.floor((seconds - (h * 3600)) / 60);
            var s = seconds - (h * 3600) - (m * 60);
            var cs = 99;

            if (h < 10) { h = "0" + h; }
            if (m < 10) { m = "0" + m; }
            if (s < 10) { s = "0" + s; }

            // console.log(h+':'+m+':'+s);
            var interval = setInterval(function () {
                $(".show_hours").text(h);
                $(".show_minutes").text(m);
                $(".show_seconds").text(s);
                s--;
                if (s == 0) {
                    if (m > 0) {
                        m--;
                        // console.log(m);
                        s = 61;
                        s--;
                        // console.log(s);
                        return m;
                    } else if (m == 0) {
                        if (h > 0) {
                            h--;
                            // console.log(h);
                            m = 59;
                            s = 61;
                            s--;
                            // console.log(s);
                            return h;
                            console.log(h);
                        } else if (h == 0) {
                            clearInterval(interval);
                        }
                    }
                } else {
                    return;
                }
            }, 1000);
            var interval2 = setInterval(function () {
                cs--;
                $(".show_cemi_seconds").text(cs);
                // console.log(cs);
                if(cs == 0){
                    if(s == 0){
                        clearInterval(interval2);
                    }
                }
            }, 100);

        });
    });
    $(document).ready(function () {
        $(".btn_pause").click(function () {
            $(".status").val("PAUSE AT: ").css("color", "blue").css("border-color", "blue").stop();
            window.clearInterval();
        });
    });
    $(document).ready(function () {
        $(".btn_reset").click(function () {
            $(".show_cemi_seconds").val("");
            console.log()
        });
    });
</script>